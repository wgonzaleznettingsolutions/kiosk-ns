new WOW().init();

$(document).ready(function() {
	$('#keyword').live('keyboard',function(){
		layout: 'custom',
		autoAccept:true,
		display : {
			'accept': 'Enter:Aceptar (Shift-Enter)',
			'cancel': 'Cancelar:Cancelar (Esc)',
			'bksp': '\u2190:Backspace'
		},
		visible: function(){
			$('.ui-keyboard').css({
				'z-index': '100001'
			});
		},
		change: function(e){
			stopTime();
			search($('#keyword').val());
		},            
		customLayout: {
			'default': [
				'1 2 3 4 5 6 7 8 9 0 {bksp}',
				'q w e r t y u i o p ',
				'a s d f g h j k l \'',
				'z x c v b n m , .',
				'{cancel} {space} {accept}'
			]
		},
		usePreview: false,
		repeatRate: 0,
		collision: 'fit',
		position:{
			of: '#search',
			my: 'left top',
			at: 'center bottom',
			at2: 'left bottom'
		}
	});
	
	$('body').keypress(function(e){
		var keyboard = $('#keyword').getkeyboard();
		if ( e.which == 13 ) {
			e.preventDefault();
			keyboard.accept();
		}
	});
});