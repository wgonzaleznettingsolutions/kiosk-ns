<?php
class Ninetwo_Categoryslider_Block_Categoryslider extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getCategoryslider()     
     { 
        if (!$this->hasData('categoryslider')) {
            $this->setData('categoryslider', Mage::registry('categoryslider'));
        }
        return $this->getData('categoryslider');
        
    }
	
	public function getProductautosearch()
	{
	
		$term = $this->getRequest()->getParam('term');
		$query = Mage::getModel('catalogsearch/query')->setQueryText($term)->prepare();
		$fulltextResource = Mage::getResourceModel('catalogsearch/fulltext')->prepareResult(
			Mage::getModel('catalogsearch/fulltext'),
			$term,
			$query
		);

		$collection = Mage::getResourceModel('catalog/product_collection');
		$collection->getSelect()->joinInner(
			array('search_result' => $collection->getTable('catalogsearch/result')),
			$collection->getConnection()->quoteInto(
				'search_result.product_id=e.entity_id AND search_result.query_id=?',
				$query->getId()
			),
			array('relevance' => 'relevance')
		);
		//$res_html = "";
		$responseArray = array();
		$productIds = array();
		$productIds = $collection->getAllIds(); // as per Amit Bera s' comment
		$productObj = Mage::getModel('catalog/product');
		$res_html.="<ul>";
		$p_count = 0;
		foreach($productIds as $productId){
			$eachProductArray = array();
			$productData = $productObj->load($productId);
			$imgSrc = Mage::helper('catalog/image')->init($productData, 'thumbnail')->resize(60);
			//imgSrc = Mage::helper('catalog/image')->init($productData, 'thumbnail')->resize(100);
			//$eachProductArray['productName'] = $productData->getName();
			$productName = $productData->getName();
			$productPath = Mage::getBaseUrl() . $productData->getUrlPath();
			//$responseArray[$p_count] = $eachProductArray;
			//$eachProductArray++;
			 $res_html.="<li style='float:left; width:100%;'>";
			$res_html.="<img style='float:left;' src='$imgSrc' />";
			$res_html.="<a style='float: left; padding: 20px 0px 0px 30px; font-size: 16px; line-height: 17px;' href='$productPath'>$productName</a>";
			$res_html.="</li>"; 
			//echo "<pre>"; print_r($productData->toArray()); echo "</pre>";
		}
		$res_html.="</ul>";
		echo $res_html;
		//$responseArray['res_html'] = $res_html;
		//echo json_encode($responseArray);
		//echo "<dl>"; echo "Hello";echo  "</dl>";
		
		
		//Zend_Debug::dump($productIds);
	}
}