<?php

class Proximity_Category_Block_Adminhtml_Categorybackend_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('category_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle('Update/Create Category');
    }

    protected function _beforeToHtml() {
        $this->addTab('form_section', array(
            'label' => 'Category information',
            'title' => 'Category information',
            'content' => $this->getLayout()
                    ->createBlock('category/adminhtml_categorybackend_edit_tab_form')
                    ->toHtml()
        ));
        return parent::_beforeToHtml();
    }

}
