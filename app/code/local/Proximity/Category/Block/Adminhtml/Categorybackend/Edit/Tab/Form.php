<?php

class Proximity_Category_Block_Adminhtml_Categorybackend_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $latlon = array();
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('category_form', array('legend' => 'category information'));
        $fieldset->addField('title', 'text', array(
            'label' => 'Category Title',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'title',
        ));
        $fieldset->addField("description", "editor", array(
            "label" => Mage::helper("adminhtml")->__("Category Description"),
            "class" => "required-entry",
            "required" => true,
            "style" => "height:15em",
            "name" => "description",
            "config" => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
            "wysiwyg" => true,
        ));
        $fieldset->addField('publish', 'select', array(
            'name' => 'publish',
            'label' => 'Publish Category',
            'id' => 'publish',
            'title' => 'Publish Category',
            'required' => true,
            'values' => array('1' => 'Yes', '0' => 'No'),
        ));
        
        if (Mage::registry('category_data')) {
            $form->setValues(Mage::registry('category_data')->getData());
        }
        return parent::_prepareForm();
    }

}
