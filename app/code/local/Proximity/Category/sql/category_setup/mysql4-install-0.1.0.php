<?php

$installer = $this;
$installer->startSetup();
$sql = <<<SQLTEXT
CREATE TABLE `proximity_category` (
	`category_id` INT(50) NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(50) NOT NULL,
	`description` TEXT NOT NULL,
	`publish` TINYINT(5) NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
        PRIMARY KEY (`category_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
SQLTEXT;

$installer->run($sql);
$installer->endSetup();
