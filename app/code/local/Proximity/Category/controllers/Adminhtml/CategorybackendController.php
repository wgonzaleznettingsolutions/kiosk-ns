<?php

class Proximity_Category_Adminhtml_CategorybackendController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        $this->getLayout()->createBlock('category/adminhtml_categorybackend');
        $this->loadLayout();
        $this->_title($this->__("Manage Category"));
        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function editAction() {
        $testId = $this->getRequest()->getParam('category_id');
        $categoryModel = Mage::getModel('category/category')->load($testId);
        if ($categoryModel->getId() || $testId == 0) {
            Mage::register('category_data', $categoryModel);
            $this->loadLayout();
            $this->getLayout()->getBlock('head')
                    ->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()
                            ->createBlock('category/adminhtml_categorybackend_edit'))
                    ->_addLeft($this->getLayout()
                            ->createBlock('category/adminhtml_categorybackend_edit_tabs')
            );
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')
                    ->addError('Category does not exist');
            $this->_redirect('*/*/');
        }
    }

    public function saveAction() {
        if ($this->getRequest()->getPost()) {
            try {
                $postData = $this->getRequest()->getPost();
                $categoryModel = Mage::getModel('category/category');
                if ($this->getRequest()->getParam('category_id') <= 0)
                    $categoryModel->setCreatedTime(
                            Mage::getSingleton('core/date')
                                    ->gmtDate()
                    );
                $categoryModel
                        ->addData($postData)
                        ->setUpdateTime(
                                Mage::getSingleton('core/date')
                                ->gmtDate())
                        ->setId($this->getRequest()->getParam('category_id'))
                        ->save();
                Mage::getSingleton('adminhtml/session')
                        ->addSuccess('Category Information Successfully Saved');
                Mage::getSingleton('adminhtml/session')
                        ->settestData(false);
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')
                        ->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')
                        ->settestData($this->getRequest()
                                ->getPost()
                );
                $this->_redirect('*/*/edit', array('category_id' => $this->getRequest()
                            ->getParam('category_id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam('category_id') > 0) {
            try {
                $clientModel = Mage::getModel('category/category');
                $clientModel->setId($this->getRequest()
                                ->getParam('category_id'))
                        ->delete();
                Mage::getSingleton('adminhtml/session')
                        ->addSuccess('Category Successfully Deleted');
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')
                        ->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('category_id' => $this->getRequest()->getParam('category_id')));
            }
        }
        $this->_redirect('*/*/');
    }

}
