<?php

$installer = $this;
$installer->startSetup();
$sql = <<<SQLTEXT
ALTER TABLE `proximity_client`
	CHANGE COLUMN `beacon_id` `beacon_id` INT NOT NULL AFTER `lon`;
SQLTEXT;

$installer->run($sql);
$installer->endSetup();
