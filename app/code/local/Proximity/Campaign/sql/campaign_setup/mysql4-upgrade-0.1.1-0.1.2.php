<?php

$installer = $this;
$installer->startSetup();
$sql = <<<SQLTEXT
    ALTER TABLE `proximity_campaign`
	ADD COLUMN `beacon_ids` VARCHAR(50) NULL AFTER `unpublish_at`;
SQLTEXT;

$installer->run($sql);
$installer->endSetup();
