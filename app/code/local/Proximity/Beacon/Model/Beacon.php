<?php

class Proximity_Beacon_Model_Beacon extends Mage_Core_Model_Abstract {

    protected function _construct() {

        $this->_init("beacon/beacon");
    }

    public function getPublishBeacons() {

        $collection = $this->getCollection()->addFieldToFilter('status', '1');
        $beacons = $collection->getData();
        $data = array();

        foreach ($beacons as $beacon) {
            $data[$beacon['beacon_id']] = $beacon['beacon_name'];
        }

        return $data;
    }

    public function getBeaconName($id) {

        $data = $this->load($id);
        $beaconname = $data->getData('beacon_name');

        return $beaconname;
    }

    public function getPublishBeaconsMultiselect() {

        $collection = $this->getCollection()->addFieldToFilter('status', '1');
        $beacons = $collection->getData();
        $data = array();

        foreach ($beacons as $beacon) {
            array_push($data, array('label' => $beacon['beacon_name'], 'value' => $beacon['beacon_id']));
        }

        return $data;
    }

}
