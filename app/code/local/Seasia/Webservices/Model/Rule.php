<?php 

class Seasia_Webservices_Model_Rule extends Mage_SalesRule_Model_Rule
{
	
	/**
	 * Save/delete coupon
	 *
	 * @return Mage_SalesRule_Model_Rule
	 */
	protected function _afterSave()
	{
		
		$couponCode = trim($this->getCouponCode());
		if (strlen($couponCode)
				&& $this->getCouponType() == self::COUPON_TYPE_SPECIFIC
				&& !$this->getUseAutoGeneration()
		) {
			 
			$this->getPrimaryCoupon()
			->setCode($couponCode)
			->setUsageLimit($this->getUsesPerCoupon() ? $this->getUsesPerCoupon() : null)
			->setUsagePerCustomer($this->getUsesPerCustomer() ? $this->getUsesPerCustomer() : null)
			->setExpirationDate($this->getToDate())
			->save();
			//generate pass file
	
			$coupon_id = $this->getPrimaryCoupon()->getData()['coupon_id'];
			/* generate passbokk for every file*/
	
			$rule_obj = Mage::getModel('salesrule/rule')->load($this->getRule_id());
			$rule_obj->getDiscountAmount();
	
			$root_path = Mage::getBaseDir('lib');
			$image_path = Mage::getBaseDir('skin')."/adminhtml/default/default/images/passbook_images/";
			$TempPath = Mage::getBaseDir()."/Passes";
	
			//generate pass.json for coupon
			require_once(Mage::getBaseDir('lib') . '/Passbook/PassBookPass.php');
			require_once(Mage::getBaseDir('lib') . '/Passbook/pkValidate.php');
	
			$obj2 = new PassBook\Pass(COUPON,"pass.com.GeoTrack","HE7TDTH6H9","Beacon General Store","Beacon General Store Coupon","Super Store","rgb(22, 55, 110)","rgb(50, 91, 185)");
			$obj2->addBarCode($couponCode);
	
			$obj2->addLocation(27.43903,69.4532322);
			$obj2->addLocation(88.85342,33.7540967);
			$obj2->couponPass_addHeaderInfo("No","6901");
			$obj2->couponPass_addDesc("on all beacons",$rule_obj->getDiscountAmount());
			$obj2->couponPass_addInfo("Expiry:",$this->getToDate());
			//$obj2->couponPass_addInfo("Member:","John");
			$obj2->addIcon($image_path."icon.png");
			$obj2->addIconHD($image_path."icon@2x.png");
			$obj2->addLogo($image_path."logo.png");
			$obj2->addLogoHD($image_path."logo@2x.png");
			$obj2->addBackScreenInfo("Copyrights","All rights reserved Super Beacon Store");
	
	
			$JSON = $obj2->getpassData();
			//$JSON = file_get_contents($root_path."/Passbook/pass.json");
	
	
			//GENERATE PKPASS FILE
			require_once(Mage::getBaseDir('lib') . '/Passbook/passkit.php');
			$wwdr_pem_file = $root_path."/certificates/WWDR1.pem";
			$passbook_cert = $root_path."/certificates/passkit.p12";
	
			$Certificates = array('AppleWWDRCA'  => $wwdr_pem_file,
					'Certificate'  => $passbook_cert,
					'CertPassword' => 'seasia@123');
	
	
	
			$ImageFiles = array($image_path.'icon.png', $image_path.'icon@2x.png', $image_path.'logo.png',$image_path.'logo@2x.png');
	
			 
	
			$pkPass_File_Link = echoPass(createPass($couponCode,$Certificates, $ImageFiles, $JSON, 'couponPass', $TempPath));
	
			if($this->getId())
			{
	
				//database read adapter
				$read = Mage::getSingleton('core/resource')->getConnection('core_read');
	
				//database write adapter
				$write = Mage::getSingleton('core/resource')->getConnection('core_write');
				$write->update(
						"salesrule_coupon",
						array("passlink" => $pkPass_File_Link),
						"coupon_id=".$coupon_id
				);
			}
			//ends here
	
	
		} else {
			$this->getPrimaryCoupon()->delete();
		}
	
		parent::_afterSave();
		return $this;
	}
}