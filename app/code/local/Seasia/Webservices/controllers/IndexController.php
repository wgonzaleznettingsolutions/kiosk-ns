<?php
class Seasia_Webservices_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        echo 'Hello developer...';
        if($_GET['login']){
        	
        	echo $this->loginAction();
        }
    }
 
    /**
     * function to create login for customer
     * 
     * @author rkaur3
     * @version 1.0
     */
    public function loginAction()
    {
    	// action body
    	$handle = fopen('php://input', 'r' );
    	$jsonInput = fgets ( $handle );
		//$jsonInput = '{"email":"sweet@yopmail.com","password":"123456","device_token":"test"}';
		
    	$params = Zend_Json::decode ( $jsonInput );
		$email=$params["email"];
    	$password=$params["password"];
    	Mage::getModel('webservices/webservices')->authenticate( $email, $password, $params['device_token'] );
    } 
    
    /**
     * User Registration
     * 
     */
    public function registerUserAction()
    {
    	$handle = fopen('php://input', 'r' );
    	$jsonInput = fgets ( $handle );
		/*$jsonInput = '{"facebook_id":"82693544406628","password":"mind","wantpartner":"0","lastname":"","device_token":"f1d84f7f6","user_type":"facebook","email":"","firstname":""}';*/
		
		$params = Zend_Json::decode ( $jsonInput );
		//echo '<pre>'; print_r($params); echo '</pre>';
    	Mage::getModel('webservices/webservices')->registerUser( $params );
    }
	
    /**
     * Log out
     *
     */
    public function logoutAction()
    {
    	$handle = fopen('php://input', 'r' );
    	$jsonInput = fgets ( $handle );
    	$params = Zend_Json::decode ( $jsonInput );
    	$user_id = $params['user_id'];
    	if($user_id != "")
    	{
	    	$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
	    	$connection->beginTransaction();
	    	
	    	$select = $connection->select()
	    	->from('customer_data', array('device_token'))
	    	->where('customer_id=?',$user_id);
	    	$rowsArray = $connection->fetchAll($select);
	    	if(!empty($rowsArray))
	    	{
		    	$__fields = array();
		    	$__fields['device_token'] = null;
		    	$__where = $connection->quoteInto('customer_id =?', $user_id);
		    	$connection->update('customer_data', $__fields, $__where);
		    	$connection->commit();
		    
	    		$result = array("Status"=>200,"CustomerId"=>$user_id,"message"=>"Logged out successfully");
	    	}
    	}
    	else 
    	{
    		$result = array("Status"=>301,"Error"=>"Missing parameters");
    	}
    	$data = Zend_Json::encode($result);
    	echo $data;
    	exit();
    }
    
    /**
     * forgot password 
     * 
     */
    
    public function forgotPasswordAction()
    {
    	$handle = fopen('php://input', 'r' );
    	$jsonInput = fgets ( $handle );
    	$params = Zend_Json::decode ( $jsonInput );
    	if($params['email'] != "")
    	{
    		$customer = Mage::getModel('customer/customer')
    		->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
    		->loadByEmail($params['email']);
    		
    		if ($customer->getId()) 
    		{
    			$newResetPasswordLinkToken =  Mage::Helper('customer')->generateResetPasswordLinkToken();
    			$customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
    			$customer->sendPasswordResetConfirmationEmail();
    			$result = array("Status"=>200,"Success"=>"Email to reset password has been sent.Please check your email");
    		}
    		else 
    		{
    			$result = array("Status"=>301,"Error"=>"Please enter registered email");
    		}
    	}
    	else
    	{
    		$result = array("Status"=>301,"Error"=>"Missing parameters");
    	}
    	$data = Zend_Json::encode($result);
    	echo $data;
    	exit();
    }
    
    /**
     * welcome screen
     * 
     */
    public function welcomeScreenAction()
    {
		
		$str = Mage::app()->getLayout()->createBlock('cms/block')->setBlockId('mobile_dashboard_id')->toHtml();
		$dom=new DOMDocument();
		$dom->loadHTML($str);		
		// fetch welcome screen image
		$imgfind=$dom->getElementsByTagName('img');
		foreach($imgfind as $img):
			$imgSrc[] = $img->getAttribute('src');
		endforeach;
		
		//GET WELCOME SCREEN PROMOCODE rule id is 3 
		$oRule = Mage::getModel('salesrule/rule')->load(3);
		$pdata = $oRule->getData();
		
		
		//get pass link of coupon
		$coupon_id_passlink = $oRule->getPrimaryCoupon()->getData()['passlink'];
		
		$coupon_id = $oRule->getPrimaryCoupon()->getData()['coupon_id'];
		
		
		//fetch block title
		$title = Mage::getModel('cms/block')->setStoreId(Mage::app()->getStore()->getId())->load('mobile_dashboard_id')->getTitle();
		$result = array("Status"=>200,"message"=>"Success","welcomeText"=>$title,"image"=>$imgSrc,"promo_code"=>$pdata['coupon_code'],"html_content"=>$str,"passlink"=>$coupon_id_passlink,"coupon_id"=>$coupon_id,"validity_date"=>$pdata['to_date']);
		echo $data = Zend_Json::encode($result);
    	exit();
    }
    
    /**
     * get all beacons
     * 
     */
    public function getAllBeaconsAction()
    {
    	$beacons = Mage::getModel('webservices/webservices')->getAllBeacons();

       if($beacons)
       {
       		$result = array("Status"=>200,"message"=>"Success","beacons"=>$beacons);
       }
       else
       {
       		$result = array("Status"=>200,"message"=>"Success","beacons"=>"No records found");
       }
       echo $data = Zend_Json::encode($result);
       exit();
    }
    

    /**
     * get all beacons
     * 
     */
    public function syncAllBeaconsAction()
    {

        $handle = fopen('php://input', 'r' );
        $jsonInput = fgets ( $handle );
     
        //$jsonInput = '{"date":"2016-06-30"}';


        $params = Zend_Json::decode ( $jsonInput );
        


        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        
        $connection->beginTransaction();
        
        $select = $connection->select()
        ->from('seasia_beacon_beacon', array("entity_id", "name", "beacontype", 
            "description", "beacon_number", "uuid", "major", "minor", "category", 
            "vendor", "refresh_rate" , "message", "temperature", "batteryLife",
            "latitude", "longitude", "status", "updated_at", "created_at"));
       
        if( !empty($params) && $params['date'] != "0")
        {
            $date = $params['date'];
            $select->where(' DATE(updated_at) >= "'.$date.'"');
        }
        $select->where('status=1');
        $beacons = $connection->fetchAll($select);

        /*echo '<pre>';
        print_r($beacons);
        die;*/

        $connection->commit();


       if($beacons)
       {
            $result = array("Status"=>200,"message"=>"Success","beacons"=>$beacons, 
                'timestamp' => date('Y-m-d'));
       }
       else
       {
            $result = array("Status"=>200,"message"=>"Success","beacons"=>"No records found", 'timestamp' => date('Y-m-d'));
       }
       echo $data = Zend_Json::encode($result);
       exit();
    }








 /**
     * get all promocodes
     */
    public function syncAllPromocodesAction()
    {

        $handle = fopen('php://input', 'r' );
        $jsonInput = fgets ( $handle );
     
       // $jsonInput = '{"date":"2016-06-30"}';


        $params = Zend_Json::decode ( $jsonInput );
        


        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        
        $connection->beginTransaction();
        
        $select = $connection->select()
                ->from('seasia_beacon_promocodes', array('*'));
      
       
        if( !empty($params) && $params['date'] != "")
        {
             $date = $params['date'];
            $select->where(' DATE(updated_at) >= "'.$date.'"');
        }
      //  $select->where('status=1');
        $codes = $connection->fetchAll($select);

        foreach($codes as $key => $code){
            $codes[$key]['promocode_id'] = $codes[$key]['id'];
            unset($codes[$key]['id']);
        }

        // echo '<pre>'; print_r($codes);        die;

        $connection->commit();


       if($codes)
       {
            $result = array("Status"=>200,"message"=>"Success","promocodes"=>$codes, 
                'timestamp' => date('Y-m-d'));
       }
       else
       {
            $result = array("Status"=>200,"message"=>"Success","promocodes"=>"No records found", 'timestamp' =>date('Y-m-d'));
       }
       echo $data = Zend_Json::encode($result);
       exit();
    }




    /**
     * get beacon data
     * 
     */
    public function getBeaconDetailsAction()
    {
    	$handle = fopen('php://input', 'r' );
    	$jsonInput = fgets ( $handle );
    	$params = Zend_Json::decode ( $jsonInput );
    	if($params['beacon_id'] != "")
    	{	
    		$beacon = Mage::getModel('webservices/webservices')->getBeaconDetails( $params['beacon_id'] );
    		if($beacon)
    		{
    			$result = array("Status"=>200,"message"=>"Success","beacon_detail"=>$beacon);
    		}
    		else
    		{
    			$result = array("Status"=>301,"Error"=>"Invalid Beacon");
    		}
    	}
    	else
    	{
    		$result = array("Status"=>301,"Error"=>"Missing parameters");
    	}
    	echo $data = Zend_Json::encode($result);
       exit();
    }
    
    
    /**
     * get beacons of customer
     * 
     */
    public function getBeaconsOfVendorsAction()
    {
    	$handle = fopen('php://input', 'r' );
    	$jsonInput = fgets ( $handle );
    	$params = Zend_Json::decode ( $jsonInput );
    	if($params['user_id'] != "")
    	{
    		$beacons = Mage::getModel('webservices/webservices')->getAllBeacons( $params['user_id'] );

    		if($beacons)
    		{
    			$result = array("Status"=>200,"message"=>"Success","beacon_detail"=>$beacons);
    		}
    		else
    		{
    			$result = array("Status"=>301,"Error"=>"No beacons found");
    		}
    	}
    	else
    	{
    		$result = array("Status"=>301,"Error"=>"Missing parameters");
    	}
    	echo $data = Zend_Json::encode($result);
    	exit();
    }
    
    /**
     * Manage frequency of beacons
     * 
     * @author rkaur3
     * @version 1.0
     */
    public function manageFrequencyAction($beacon_id,$user_id,$client_time)
    {
		if($beacon_id!="" && $user_id !="" && $client_time != "")
    	{
    		//get refresh rate of beacon
	    	$beacon_obj = Mage::getSingleton('seasia_beacon/beacon')->load($beacon_id);
	   		$beacon_mesage = $beacon_obj->getData()['message'];
	   
	    	$refresh_rate_mins = $beacon_obj->getData()['refresh_rate'];
		    $time = new DateTime($client_time);
		    $time->add(new DateInterval('PT' . $refresh_rate_mins . 'M'));
	    
	    	$notification_time = $time->format('Y-m-d H:i:s');
	    
		    //check if user_id and beacon already exist in table
		    $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
		    $read = Mage::getSingleton('core/resource')->getConnection('core_read');
		    $connection->beginTransaction();
		    $tablePrefix = (string) Mage::getConfig()->getTablePrefix();
	    
		    //get device token
		    $select = $connection->select()
		    ->from('customer_data', array('device_token'))
		    ->where('customer_id=?',$user_id);
		    $devicetoken_data = $connection->fetchAll($select);
		    $device_token = $devicetoken_data[0]['device_token'];

	    
		    //get data from freq manager table
		    $select = $connection->select()
		    ->from('seasia_beacon_frequencymgr', array('*'))
		    ->where('user_id=?',$user_id)
		    ->where('beacon_id=?',$beacon_id);
		    $rowsArray = $connection->fetchAll($select);
	   
	    	//update notification and client time
	   	 	if(!empty( $rowsArray))
	    	{
				//database read adapter
				$select = "select * from seasia_beacon_frequencymgr where user_id=".$user_id." and beacon_id=".$beacon_id." and client_time < '".date($client_time)."' and notification_time > '".date($client_time)."'";
			    $freq_data = $read->fetchAll($select);
			
		    	if(empty($freq_data[0]))
				{
					// update beacon freqmgr table
					$sql = "update seasia_beacon_frequencymgr set client_time = '".date($client_time)."',notification_time = '".date($notification_time)."' where user_id=".$user_id." and beacon_id=".$beacon_id;
					$update = $connection->query($sql);
					
					//send push notification
					$pushNoitfy = Mage::getModel('webservices/webservices')->pushnotificationCrewApns($device_token,$beacon_mesage );
				}
				else
				{
					
				}
			}
	   		//for first time add data into table 
	    	else
	    	{
		    	$__fields = array();
				$__fields['user_id'] = $user_id;
				$__fields['beacon_id'] = $beacon_id;
				$__fields['client_time'] = date($client_time);
				$__fields['notification_time'] = date($notification_time);
				$connection->insert('seasia_beacon_frequencymgr', $__fields);
				
				//send push notification
				$pushNoitfy = Mage::getModel('webservices/webservices')->pushnotificationCrewApns($device_token,$beacon_mesage );
				
			}
			$connection->commit();
    	}
    	
	 }
	 
	 public function dummySignUpAction()
	 {
	 
	 	$handle = fopen('php://input', 'r' );
	 	$jsonInput = fgets ( $handle );
	 	$params = Zend_Json::decode ( $jsonInput );
	 	
	 /*	//GET WELCOME SCREEN PROMOCODE rule id is 3
	 	//$oRule = Mage::getModel('salesrule/rule')->load(3);
	 	$oRule = Mage::getModel('salesrule/rule')->load(6);
	 	$pdata = $oRule->getData();
	 	
	 	print_r(unserialize($pdata['conditions_serialized']));
	 	$customer_id = 81; // set this to the ID of the customer.
	 	$customer_data = Mage::getModel('customer/customer')->load($customer_id);
	 	 
	 	
	 	$conditions = unserialize($pdata['conditions_serialized']);
	 	if(isset($conditions['conditions']))
	 	{
	 		//die('conditions exist');
	 		foreach($conditions['conditions'] as $cond)
	 		{
	 			echo "<pre>";
	 			print_r($cond);
	 			die();
	 		}
	 		
	 	}
	 	else
	 	{
	 		die('condition does not exist');
	 	}
	 	*/
	 	//add gender to current register function
	 	Mage::getModel('webservices/webservices')->registerUserDummy( $params );
	 }
	 
	public function updateBeaconDetailsAction()
    {
    	$handle = fopen('php://input', 'r' );
    	$jsonInput = fgets ( $handle );
    	$params = Zend_Json::decode ( $jsonInput );
    	
    	$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
		$connection->beginTransaction();
    	 
    	if($params['beacon_id'] != "")
    	{	
    		$beacon = Mage::getModel('webservices/webservices')->getBeaconDetails( $params['beacon_id'] );
    		if($beacon)
    		{
    			$array = array(); 
    			$array['temperature'] = isset($params['temperature'])?$params['temperature']:'';
    			$array['acceleration'] = isset($params['acceleration'])?$params['acceleration']:'';
    			$array['batteryLife'] = isset($params['batteryLife'])?$params['batteryLife']:'';
    			$array['latitude'] = isset($params['latitude'])?$params['latitude']:'';
    			$array['longitude'] = isset($params['longitude'])?$params['longitude']:'';
    			$array['uuid'] = isset($params['uuid'])?$params['uuid']:'';
    			$array['major'] = isset($params['major'])?$params['major']:'';
    			$array['minor'] = isset($params['minor'])?$params['minor']:'';
    			/*
    			$sql = "update seasia_beacon_beacon set";
    			
    			foreach($array as $key=>$value){
					if($value!=''){
						$sql .=$key ." = '".$value."'";	
					}
				}
    			
    			$sql .="where entity_id =".$beacon_id;
    			
				$update = $connection->query($sql);
				 */
		    	$__where = $connection->quoteInto('entity_id =?', $params['beacon_id']);
		    	$connection->update('seasia_beacon_beacon', $array, $__where);
		    	$connection->commit();
		    	
    			$result = array("Status"=>200,"message"=>"Beacon Saved Successfully");
    		}
    		else
    		{
    			$result = array("Status"=>301,"Error"=>"Invalid Beacon");
    		}
    	}
    	else
    	{
    		$result = array("Status"=>301,"Error"=>"Missing parameters");
    	}
    	echo $data = Zend_Json::encode($result);
       exit();
    }
    
    
    public function welcomeBeaconScreenAction(){
		$handle = fopen('php://input', 'r' );
    	$jsonInput = fgets ( $handle );
    	$params = Zend_Json::decode ( $jsonInput ); 
    	
    	$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
		$connection->beginTransaction();
		
		if($params['beacon_id'] != "")
    	{	
    		$beacon = Mage::getModel('webservices/webservices')->getBeaconDetails( $params['beacon_id'] );
    		if($beacon)
    		{
    			//get coupon data of beacon
    			$select = $connection->select()
    			->from('seasia_beacon_promocodes', array('*'))
    			->where('beacon_id=?',$beacon['entity_id']);
    			$connection->commit();
    			$promArray = $connection->fetchAll($select);
    			
    			if(is_array($promArray) && count($promArray)>0){
					
					/* get coupon details according to coupon id*/
					$collection = Mage::getResourceModel('salesrule/coupon_collection');
					$collection->addFieldToFilter('coupon_id',array('eq'=>$promArray[0]['coupon_id']));
					$cData = $collection->getData(); 
					
					$rule = Mage::getModel('salesrule/rule')->load($cData[0]['rule_id']);
					  
					
					
					$beaconPromocodeArray['discount'] = $rule->getDiscountAmount();
					$beaconPromocodeArray['validity_date'] = $rule->getTo_date();
					$beaconPromocodeArray['passlink'] = $cData[0]['passlink'];
					$beaconPromocodeArray['coupon_id'] = $promArray[0]['coupon_id'];
					$beaconPromocodeArray['welcomeText'] = $promArray[0]['promo_message'];
					$beaconPromocodeArray['coupon_code'] = $cData[0]['code'];
					$beaconPromocodeArray['message'] = $promArray[0]['promo_message'];
					
					if($promArray[0]['promo_image'] != ""){
						$beaconPromocodeArray['image'] = Mage::getBaseUrl('media')."promo_images/". $promArray[0]['promo_image'];
					}else{
						$beaconPromocodeArray['image'] = "No image";
					}
										
					$beaconArray = array_merge($beacon,$beaconPromocodeArray);
				}else{
					
					$beaconPromocodeArray['discount'] = '';
					$beaconPromocodeArray['welcomeText'] = '';
					$beaconPromocodeArray['validity_date'] = '';
					$beaconPromocodeArray['passlink'] = '';
					$beaconPromocodeArray['coupon_id'] = '';
					$beaconPromocodeArray['coupon_code'] = '';
					$beaconPromocodeArray['message'] = '';
					$beaconPromocodeArray['image'] = "No image";
										
					$beaconArray = array_merge($beacon,$beaconPromocodeArray);
				}  
    			$result = array("Status"=>200,"beacon_promocode_data"=>$beaconArray);
    		}
    		else
    		{
    			$result = array("Status"=>301,"Error"=>"Invalid Beacon");
    		}
    	}
    	else
    	{
    		$result = array("Status"=>301,"Error"=>"Missing parameters");
    	}
    	echo $data = Zend_Json::encode($result);
       exit();
		
			
	}
	
	public function fetchallbeaconAction()  
	{
		$handle = fopen('php://input', 'r' );
    	$jsonInput = fgets ( $handle );
    	$params = Zend_Json::decode ( $jsonInput );
		
		$beacon_array = array();
		
    	 
		//get beacon id and its data
		$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
		
		$connection->beginTransaction();
		
		$select = $connection->select()
		->from('seasia_beacon_beacon', array("entity_id", "name", "beacontype", 
            "description", "beacon_number", "uuid", "major", "minor", "category", 
            "vendor", "refresh_rate" , "message", "temperature", "batteryLife",
            "latitude", "longitude", "status", "updated_at", "created_at"));
        $select->where('status=1');
		$connection->commit();
		$rowsArray = $connection->fetchAll($select);
		
		for($i=0; $i<=count($rowsArray); $i++){
			$bData = $rowsArray[$i];
			if($bData['entity_id'] != "")
			{
				/*
				$beacon_array[$i]['beacon_id'] = $bData['entity_id'];
				$beacon_array[$i]['beacon_name'] = $bData['name'];
				$beacon_array[$i]['uuid'] = $bData['uuid'];
				$beacon_array[$i]['major'] = $bData['major'];
				$beacon_array[$i]['minor'] = $bData['minor'];
				$beacon_array[$i]['proximity'] = $bData['proximity'];
				*/
				$beacon_array[$i]['beacon_id'] = $bData['entity_id'];
				$beacon_array[$i]['beacon_name'] = $bData['name'];
				$beacon_array[$i]['uuid'] = $bData['uuid'];
				$beacon_array[$i]['major'] = $bData['major'];
				$beacon_array[$i]['minor'] = $bData['minor'];
				//
                //$beacon_array[$i]['proximity'] = $bData['proximity'];
				
				/*code for push notification starts from here*/
				require_once('Seasia/Webservices/controllers/IndexController.php');
				$controller         = new Seasia_Webservices_IndexController(
						Mage::app()->getRequest(),
						Mage::app()->getResponse()
				);
				//call the action
				
				$controller->manageFrequencyAction($beacon_array[$i]['beacon_id'],$params['user_id'],$params['client_time']);
				/*code for push notification ends here*/
			
				//get coupon data of beacon
				$select = $connection->select()
				->from('seasia_beacon_promocodes', array('*'))
				->where('beacon_id=?',$beacon_array[$i]['beacon_id']);
				$connection->commit();
				$promArray = $connection->fetchAll($select);
				
				/* get coupon details according to coupon id*/
				$collection = Mage::getResourceModel('salesrule/coupon_collection');
				$collection->addFieldToFilter('coupon_id',array('eq'=>$promArray[0]['coupon_id']));
				$cData = $collection->getData();
				
				
				$rule = Mage::getModel('salesrule/rule')->load($cData[0]['rule_id']);
				
				
				$beacon_array[$i]['discount'] = $rule->getDiscountAmount();
				$beacon_array[$i]['validity_date'] = $rule->getTo_date();
				$beacon_array[$i]['passlink'] = $cData[0]['passlink'];
				$beacon_array[$i]['coupon_id'] = $promArray[0]['coupon_id'];
				$beacon_array[$i]['coupon_code'] = $cData[0]['code'];
				$beacon_array[$i]['message'] = $promArray[0]['promo_message'];
				
				if($promArray[0]['promo_image'] != "")
				{
					$beacon_array[$i]['image'] = $promo_image_path =  Mage::getBaseUrl('media')."promo_images/". $promArray[0]['promo_image'];
				}
				else
				{
					$beacon_array[$i]['image'] = "No image";
				}
			}
		}
		 
    	$result = array("Status"=>200,"beacon_data"=>$beacon_array);
    	echo json_encode($result);
    	exit();
	} 
    
	 
	 
}
?>
