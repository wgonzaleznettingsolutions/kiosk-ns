<?php
/*
* Copyright (c) 2014 www.magebuzz.com
*/
class Seasia_Promocode_Block_Adminhtml_Promocode_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'promocode';
        $this->_controller = 'adminhtml_promocode';
        
        $this->_updateButton('save', 'label', Mage::helper('promocode')->__('Save Promocode'));
        $this->_updateButton('delete', 'label', Mage::helper('promocode')->__('Delete Promocode'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('testimonial_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'testimonial_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'testimonial_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('promocode_data') && Mage::registry('promocode_data')->getId() ) {
            return Mage::helper('promocode')->__("Edit Promocode '%s'", $this->htmlEscape(Mage::registry('promocode_data')->getName()));
        } else {
            return Mage::helper('promocode')->__('Assign Promocode');
        }
    }
}