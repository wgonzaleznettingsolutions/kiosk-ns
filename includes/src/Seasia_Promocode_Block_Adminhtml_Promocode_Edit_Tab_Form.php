<?php
/*
* Copyright (c) 2014 www.magebuzz.com
*/
class Seasia_Promocode_Block_Adminhtml_Promocode_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $form->setHtmlIdPrefix('promocode_');
        $fieldset = $form->addFieldset('promocode_form', array('legend'=>Mage::helper('promocode')->__('General Information')));

        if ( Mage::getSingleton('adminhtml/session')->getPromocodeData() )
        {
            $data = Mage::getSingleton('adminhtml/session')->getPromocodeData();
            Mage::getSingleton('adminhtml/session')->setPromocodeData(null);
        } elseif ( Mage::registry('promocode_data') ) {
            $data = Mage::registry('promocode_data')->getData();
        }
		
       
       $fieldset->addField(
       		'beacon_id',
       		'select',
       		array(
       				'label' => Mage::helper('promocode')->__('Beacon'),
       				'name'  => 'beacon_id',
       				'required'  => true,
       				'class' => 'required-entry',
       				'values'=> Mage::helper('promocode')->getAllEnabledBeacons(),
       		)
       );

       $fieldset->addField('coupon_id', 'select', array(
       		'label'     => Mage::helper('promocode')->__('Coupon'),
       		'class'     => 'required-entry',
       		'required'  => true,
       		'name'      => 'coupon_id',
       		'values'=> Mage::helper('promocode')->getCoupons(),
       ));
       
      /* $fieldset->addField('temp_range', 'select', array(
       		'label'     => Mage::helper('promocode')->__('Temperature Range'),
       		'class'     => 'required-entry',
       		'required'  => true,
       		'name'      => 'temp_range',
       		'values'=> Mage::helper('promocode')->getTempRange(),
       ));
       */
       $fieldset->addField('promo_message', 'text', array(
       		'label'     => Mage::helper('promocode')->__('Promo Message'),
       		'class'     => 'required-entry',
       		'required'  => true,
       		'name'      => 'promo_message',
       ));
       
       $fieldset->addField('promo_image', 'file', array(
       		'label'     => Mage::helper('promocode')->__('Promo Image'),
       		//'class'     => 'required-entry',
       		//'required'  => true,
       		'name'      => 'promo_image',
       ));

    	$form->setValues($data);
        return parent::_prepareForm();


    }
}