<?php
/*
* Copyright (c) 2014 www.magebuzz.com
*/
class Seasia_Promocode_Model_Mysql4_Promocode extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {    
        // Note that the customer_id refers to the key field in your database table.
        $this->_init('promocode/promocode', 'id');
    }
}
