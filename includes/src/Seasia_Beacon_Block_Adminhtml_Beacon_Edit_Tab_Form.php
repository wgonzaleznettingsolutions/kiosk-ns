<?php
/**
 * Seasia_Beacon extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Seasia
 * @package        Seasia_Beacon
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Beacon edit form tab
 *
 * @category    Seasia
 * @package     Seasia_Beacon
 * @author      Ultimate Module Creator
 */
class Seasia_Beacon_Block_Adminhtml_Beacon_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Seasia_Beacon_Block_Adminhtml_Beacon_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
    	
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('beacon_');
        $form->setFieldNameSuffix('beacon');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'beacon_form',
            array('legend' => Mage::helper('seasia_beacon')->__('Beacon'))
        );

        $fieldset->addField(
            'name',
            'text',
            array(
                'label' => Mage::helper('seasia_beacon')->__('name'),
                'name'  => 'name',
            'required'  => true,
            'class' => 'required-entry',

           )
        );
		$fieldset->addField(
            'beacontype',
            'text',
            array(
                'label' => Mage::helper('seasia_beacon')->__('Beacon Type'),
                'name'  => 'beacontype',
            'required'  => true,
            'class' => 'required-entry',

           )
        );
		 
        	
        	
        $fieldset->addField(
            'description',
            'text',
            array(
                'label' => Mage::helper('seasia_beacon')->__('description'),
                'name'  => 'description',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'beacon_number',
            'text',
            array(
                'label' => Mage::helper('seasia_beacon')->__('Beacon Number'),
                'name'  => 'beacon_number',
            'required'  => true,
            'class' => 'required-entry',

           )
        );
        
        $fieldset->addField(
            'uuid',
            'text',
            array(
                'label' => Mage::helper('seasia_beacon')->__('UUID'),
                'name'  => 'uuid',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'major',
            'text',
            array(
                'label' => Mage::helper('seasia_beacon')->__('Major'),
                'name'  => 'major',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'minor',
            'text',
            array(
                'label' => Mage::helper('seasia_beacon')->__('Minor'),
                'name'  => 'minor',
            'required'  => true,
            'class' => 'required-entry',

           )
        );
		
        //$this->getRequest()->getParam('id');
        //die();
         
        if(!$this->getRequest()->getParam('id'))
        {	
			$fieldset->addField(
					'vendor',
					'select',
					
					array(
							'label' => Mage::helper('seasia_beacon')->__('Vendor'),
							'name'  => 'vendor',
							'required'  => true,
							'class' => 'required-entry',
							'values' => Mage::helper('seasia_beacon')->getAllVendors(),
							'onchange' => 'checkSelectedVendor(this.value)',
			
					)
			)->setAfterElementHtml("
					<script type=\"text/javascript\">
					function checkSelectedVendor(selectElement)
					{
					var refresh_rate_em = document.getElementById('beacon_refresh_rate');
					var range_em = document.getElementById('beacon_proximity');
					
					var reloadurl = '". Mage::getUrl('adminhtml/beacon_beacon')."settings/parent_id/' + selectElement;
					new Ajax.Request(reloadurl, {
					method: 'post',
					onComplete: function(transport) {
						if(transport.responseText != 'false')
					{        		
						var txt = JSON.parse(transport.responseText);
						
						var beacon_range = txt.beacon_range;
						var refresh_rate = txt.refresh_rate;
					
						refresh_rate_em.value =  refresh_rate;
						refresh_rate_em.disabled = true;
						
						range_em.value =  beacon_range;
						range_em.disabled = true;
					 }
					 else
					 {
						refresh_rate_em.value =  '';
						range_em.value =  '';
						refresh_rate_em.disabled = false;
						range_em.disabled = false;
					 }
				   }
			});
			}
					</script>");
			
			
			$fieldset->addField(
				'proximity',
				'select',
				array(
					'label' => Mage::helper('seasia_beacon')->__('Proximity'),
					'name'  => 'proximity',
				'required'  => true,
				'class' => 'required-entry',

				'values'=> Mage::getModel('seasia_beacon/beacon_attribute_source_proximity')->getAllOptions(true),
			   )
			);
			$fieldset->addField(
				'refresh_rate', 'text',
				array( 'label' => Mage::helper('seasia_beacon')->__('Refresh Rate'),
						'class' => 'required-entry',
						'required' => true,
						'name' => 'refresh_rate',
						'after_element_html' => '<small>mins</small>',
						'tabindex' => 1
					)
			);
        }
       else 
        {
        	$fieldset->addField(
        			'vendor',
        			'select',
        	
        			array(
        					'label' => Mage::helper('seasia_beacon')->__('Vendor'),
        					'name'  => 'vendor',
        					'required'  => true,
        					'disabled' => true,
        					'required' => true,
        					'class' => 'required-entry',
        					'values' => Mage::helper('seasia_beacon')->getAllVendors(),
        					'onchange' => 'checkSelectedVendor(this.value)',
        	
        			)
        	)->setAfterElementHtml("
        			<script type=\"text/javascript\">
        			function checkSelectedVendor(selectElement)
        			{
        			var refresh_rate_em = document.getElementById('beacon_refresh_rate');
        			var range_em = document.getElementById('beacon_proximity');
        	
        			var reloadurl = '". Mage::getUrl('adminhtml/beacon_beacon')."settings/parent_id/' + selectElement;
        			new Ajax.Request(reloadurl, {
        			method: 'post',
        			onComplete: function(transport) {
        			if(transport.responseText != 'false')
        			{
        			var txt = JSON.parse(transport.responseText);
        			 
        			var beacon_range = txt.beacon_range;
        			var refresh_rate = txt.refresh_rate;
        	
        			refresh_rate_em.value =  refresh_rate;
        			refresh_rate_em.disabled = true;
        			 
        			range_em.value =  beacon_range;
        			range_em.disabled = true;
        	}
        			else
        			{
        			refresh_rate_em.value =  '';
        			range_em.value =  '';
        			refresh_rate_em.disabled = false;
        			range_em.disabled = false;
        	}
        	}
        	});
        	}
        			</script>");
        	
        	
        	$fieldset->addField(
        			'proximity',
        			'select',
        			array(
        					'label' => Mage::helper('seasia_beacon')->__('Proximity'),
        					'name'  => 'proximity',
        					'required'  => true,
        					'class' => 'required-entry',
        					'disabled' => true,
        					'required' => true,
        					'values'=> Mage::getModel('seasia_beacon/beacon_attribute_source_proximity')->getAllOptions(true),
        			)
        	);
        	$fieldset->addField(
        			'refresh_rate', 'text',
        			array( 'label' => Mage::helper('seasia_beacon')->__('Refresh Rate'),
        					'class' => 'required-entry',
        					'readonly' => true,
        					'disabled' => true, 
        					'required' => true,
        					'name' => 'refresh_rate',
        					'after_element_html' => '<small>mins</small>',
        					'tabindex' => 1
        			)
        	);
        }	
        	
        
        
        
        $fieldset->addField(
        		'category',
        		'select',
        		array(
        				'label' => Mage::helper('seasia_beacon')->__('Category'),
        				'name'  => 'category',
        				'required'  => true,
        				'class' => 'required-entry',
        				'values' => Mage::helper('seasia_beacon')->getAllCategoriesArray(true),
        				
        				//'values'=> Mage::getModel('seasia_beacon/beacon_attribute_source_range')->getAllOptions(true),
        		)
        );
        
        $fieldset->addField(
            'message',
            'text',
            array(
                'label' => Mage::helper('seasia_beacon')->__('Message'),
                'name'  => 'message',
            'required'  => true,
            'class' => 'required-entry',

           )
        );
        
        
        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('seasia_beacon')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('seasia_beacon')->__('Enabled'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('seasia_beacon')->__('Disabled'),
                    ),
                ),
            )
        );
        if (Mage::app()->isSingleStoreMode()) {
            $fieldset->addField(
                'store_id',
                'hidden',
                array(
                    'name'      => 'stores[]',
                    'value'     => Mage::app()->getStore(true)->getId()
                )
            );
            Mage::registry('current_beacon')->setStoreId(Mage::app()->getStore(true)->getId());
        }
        $formValues = Mage::registry('current_beacon')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getBeaconData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getBeaconData());
            Mage::getSingleton('adminhtml/session')->setBeaconData(null);
        } elseif (Mage::registry('current_beacon')) {
            $formValues = array_merge($formValues, Mage::registry('current_beacon')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
