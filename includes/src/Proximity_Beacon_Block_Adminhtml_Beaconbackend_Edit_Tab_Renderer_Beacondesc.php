<?php

class Proximity_Beacon_Block_Adminhtml_Beaconbackend_Edit_Tab_Renderer_Beacondesc extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        return strip_tags($row->getDescription());
    }

}
