<?php
/*
* Copyright (c) 2014 www.magebuzz.com
*/
class Seasia_Promocode_Block_Adminhtml_Promocode_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('promocodeGrid');
		$this->setDefaultSort('id');
		$this->setDefaultDir('ASC');
		//$this->setUseAjax(true);
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('promocode/promocode')->getCollection();

		$this->setCollection($collection);
		return parent::_prepareCollection();
	}
	
	protected function _prepareColumns()
	{
	
	  $this->addColumn('id', array(
          'header'    => Mage::helper('promocode')->__('Id'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'id',
      ));
	  
	 $this->addColumn('promo_image', array(
          'header'    => Mage::helper('promocode')->__('Promo Image'),
          'align'     =>'left',
          'index'     => 'promo_image',
	 	  'width'     => '20px',
	 	  'height'     => '20px',
          'renderer'  => 'promocode/adminhtml_promocode_renderer_Image'
      ));

	  
	  $this->addColumn('beacon_id', array(
	  		'header'    => Mage::helper('promocode')->__('Beacon'),
	  		'align'     =>'right',
	  		'width'     => '50px',
	  		'renderer'  => new Seasia_Promocode_Block_Adminhtml_Promocode_Renderer_BeaconName,
	  		'index'     => 'beacon_id',
	  ));
	  $this->addColumn('coupon_id', array(
	  		'header'    => Mage::helper('promocode')->__('Coupon Code'),
	  		'align'     =>'right',
	  		'width'     => '50px',
	  		'index'     => 'coupon_id',
	  		'renderer'  => 'promocode/adminhtml_promocode_renderer_CouponCode'
	  ));
	  $this->addColumn('promo_message', array(
	  		'header'    => Mage::helper('promocode')->__('Promo message'),
	  		'align'     =>'right',
	  		'width'     => '50px',
	  		'index'     => 'promo_message',
	  ));
	
	   $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('promocode')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
            	'actions'   => array(
            				array(
            						'caption'   => Mage::helper('promocode')->__('Edit'),
            						'url'       => array('base'=> '*/*/edit'),
            						'field'     => 'id'
            				)
            		),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		//$this->addExportType('*/*/exportCsv', Mage::helper('promocode')->__('CSV'));
		//$this->addExportType('*/*/exportXml', Mage::helper('promocode')->__('XML'));
		
	  
      return parent::_prepareColumns();
	}


	
	public function getRowUrl($row)
	{
	  return  '#'; $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}
	
	
	/**
	 * prepare mass action
	 *
	 * @access protected
	 * @return Seasia_Promocode_Block_Adminhtml_Promocode_Grid
	 * @author Ultimate Module Creator
	 */
	protected function _prepareMassaction()
	{
		$this->setMassactionIdField('id');
		$this->getMassactionBlock()->setFormFieldName('promocode');
		$this->getMassactionBlock()->addItem(
				'delete',
				array(
						'label'=> Mage::helper('promocode')->__('Delete'),
						'url'  => $this->getUrl('*/*/massDelete'),
						'confirm'  => Mage::helper('promocode')->__('Are you sure?')
				)
		);
		
		return $this;
	}
	
}