<?php
/*
* Copyright (c) 2014 www.magebuzz.com
*/
class Seasia_Promocode_Block_Adminhtml_Promocode_Renderer_CouponCode extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{
	
	public function render(Varien_Object $row)
	{
		$coupon_id = $row->getData($this->getColumn()->getIndex());
		
		$collection = Mage::getResourceModel('salesrule/coupon_collection');
		$collection->getSelect()
		->reset(Zend_Db_Select::COLUMNS)
		->columns('coupon_id as coupon_id')
		->columns('code as coupon_code');
		$collection->addFieldToFilter('coupon_id',array('eq'=>$coupon_id));
		$coupon_data = $collection->getData();
		return $coupon_data[0]['coupon_code'];
	}
	
}