<?php

class Proximity_Category_Block_Adminhtml_Categorybackend_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {

        parent::__construct();
        $this->setId('proximity_category_grid');
        $this->setDefaultSort('category_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {

        $collection = Mage::getModel('category/category')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $this->addColumn('category_id', array(
            'header' => 'ID',
            'align' => 'right',
            'width' => '50px',
            'index' => 'category_id',
        ));
        $this->addColumn('title', array(
            'header' => 'Title',
            'align' => 'right',
            'index' => 'title',
            'width' => '150px',
        ));
        $this->addColumn('description', array(
            'header' => 'Description',
            'align' => 'left',
            'index' => 'description',
            'renderer' => 'Proximity_Category_Block_Adminhtml_Categorybackend_Edit_Tab_Renderer_Categorydesc',
        ));
        $this->addColumn('publish', array(
            'header' => 'Publish',
            'align' => 'left',
            'width' => '50px',
            'index' => 'publish',
            'renderer' => 'Proximity_Category_Block_Adminhtml_Categorybackend_Edit_Tab_Renderer_Categorypublish',
        ));
        $this->addColumn('action', array(
            'header' => Mage::helper('customer')->__('Action'),
            'width' => '100',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('customer')->__('Edit'),
                    'url' => array('base' => '*/*/edit'),
                    'field' => 'category_id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'is_system' => true,
        ));
        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('category_id' => $row->getId()));
    }

}
